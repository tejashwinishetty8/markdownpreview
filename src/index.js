import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import marked from 'marked';


class Markdown extends React.Component{
    constructor(props){
        super(props);
        this.state = {
			textareaVal:
`# Welcome to my React Markdown Previewer!

## This is a sub-heading...
### And here's some other cool stuff:
  
Heres some code, \`<div></div>\`, between 2 backticks.

\`\`\`
// this is multi-line code:

function anotherExample(firstLine, lastLine) {
  if (firstLine == '\`\`\`' && lastLine == '\`\`\`') {
    return multiLineCode;
  }
}
\`\`\`
  
You can also make text **bold**... whoa!
Or _italic_.
Or... wait for it... **_both!_**
And feel free to go crazy ~~crossing stuff out~~.

There's also [links](https://www.freecodecamp.com), and
> Block Quotes!

And if you want to get really crazy, even tables:

Wild Header | Crazy Header | Another Header?
------------ | ------------- | ------------- 
Your content can | be here, and it | can be here...
And here. | Okay. | I think we get it.

- And of course, there are lists.
- Some are bulleted.
- With different indentation levels.
- That look like this.


1. And there are numbered lists too.
1. Use just 1s if you want! 
1. But the list goes on...
- Even if you use dashes or asterisks.
* And last but not least, let's not forget embedded images:

![React Logo w/ Text](https://goo.gl/Umyytc)
`
			,
			editorMaximized: false,
      		previewMaximized: false
        }
        this.handleEvent = this.handleEvent.bind(this);
		this.getMarkdownText = this.getMarkdownText.bind(this);
		this.handleEditorMaximize = this.handleEditorMaximize.bind(this);
    	this.handlePreviewMaximize = this.handlePreviewMaximize.bind(this);
    }
    handleEvent(e){
        this.setState({textareaVal: e.target.value});
    }
    getMarkdownText(){
        var text = this.state.textareaVal;
        var rawMarkup = marked(text,{sanitize: true});
        return {__html: rawMarkup};
	}
	handleEditorMaximize() {
		this.setState({
		  editorMaximized: !this.state.editorMaximized
		});
	  }
	  handlePreviewMaximize() {
		this.setState({
		  previewMaximized: !this.state.previewMaximized
		});
	  }
    
    render(){
		const classes = this.state.editorMaximized ? 
          ['editorWrap_maximized', 
           'previewWrap_hide', 
		   'fa fa-compress',
			'texteditor'] : 
          this.state.previewMaximized ?
          ['editorWrap_hide', 
           'previewWrap_maximized', 
		   'fa fa-compress',
			'',
			'PreviewMaximize'] :
          ['editorMainContainer', 
           'previewMainContainer', 
		   'fa fa-arrows-alt',
			'Editor',
			'Preview'];
        return (
            <div id="maincontainer" >
                <div id="editorMainContainer" className={classes[0]}>
                    <div id="editorBar">
                        <h4>Editor</h4>
                        <i className={classes[2]} onClick={this.handleEditorMaximize}></i>
                    </div>
                    <div>
                    <textarea id="editor" className={classes[3]} value={this.state.textareaVal} onChange= {this.handleEvent} >
                    </textarea>
                    </div>
                </div>
                <div id="previewMainContainer" className={classes[1]}>
				<div id="editorBar">
                        <h4>Preview</h4>
                        <i className={classes[2]} onClick={this.handlePreviewMaximize}></i>
                    </div>
                <div id="preview" className={classes[4]}  dangerouslySetInnerHTML={this.getMarkdownText()}>

                </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(
	<Markdown />, 
	document.getElementById('root')
)
